import { TImage } from '../types';

// Lifted from 'resize-image-data' and fixed for multiple channels

function bilinearInterpolation(srcImage: TImage, rsWidth: number, rsHeight: number): TImage {
  const dstImage: TImage = {
    width: rsWidth,
    height: rsHeight,
    channels: srcImage.channels,
    data: new Uint8ClampedArray(rsWidth * rsHeight * srcImage.channels),
  };

  function interpolate(k: number, kMin: number, kMax: number, vMin: number, vMax: number) {
    return Math.round((k - kMin) * vMax + (kMax - k) * vMin);
  }

  function interpolateHorizontal(offset: number, x: number, y: number, xMin: number, xMax: number) {
    const vMin = srcImage.data[((y * srcImage.width + xMin) * srcImage.channels) + offset];
    if (xMin === xMax) return vMin;

    const vMax = srcImage.data[((y * srcImage.width + xMax) * srcImage.channels) + offset];
    return interpolate(x, xMin, xMax, vMin, vMax);
  }

  function interpolateVertical(offset: number, x: number, xMin: number, xMax: number, y: number, yMin: number, yMax: number) {
    const vMin = interpolateHorizontal(offset, x, yMin, xMin, xMax);
    if (yMin === yMax) return vMin;

    const vMax = interpolateHorizontal(offset, x, yMax, xMin, xMax);
    return interpolate(y, yMin, yMax, vMin, vMax);
  }

  let pos = 0;
  for (let y = 0; y < dstImage.height; y++) {
    for (let x = 0; x < dstImage.width; x++) {
      const srcX = x * srcImage.width / dstImage.width;
      const srcY = y * srcImage.height / dstImage.height;

      const xMin = Math.floor(srcX);
      const yMin = Math.floor(srcY);

      const xMax = Math.min(Math.ceil(srcX), srcImage.width - 1);
      const yMax = Math.min(Math.ceil(srcY), srcImage.height - 1);

      for (let c = 0; c < srcImage.channels; c++) {
        dstImage.data[pos++] = interpolateVertical(c, srcX, xMin, xMax, srcY, yMin, yMax);
      }
    }
  }
  return dstImage;
}

export { bilinearInterpolation as resize };
