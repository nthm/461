import * as path from 'path';
import * as fs from 'fs';

import program from 'commander';
import read from 'read';
import prettyBytes from 'pretty-bytes';
import { stripIndent, oneLine, html } from 'common-tags';
import { encode, decode } from 'fast-png';

import { TImage } from './types';

import { crop } from './operations/crop';
import { resize } from './operations/resize';
import { promisify } from 'util';

process.on('unhandledRejection', event => { throw event; });

// Dicehalf has two parts; dice and half.

// Instead of using commands/actions in commander.js for these, which would be a
// branch and extra complexity, I made them implicit:
//
// - To tile only, set the zoom to zero
// - To resize only, set the tile size to zero

// OS-specific paths
const defaultOutputSpec = path.join(...'tiles/{z}/{y}/{x}.png'.split('/'));

program
  .name('dicehalf')
  .usage('-i <image> [options]')
  .version('0.0.1')

  .requiredOption('-i, --image <file>', 'image to work on (in PNG format)')
  .option('-o, --output-spec <spec>', 'output path stucture for tiles. Supported tokens are {x} {y} {z}', defaultOutputSpec)
  .option('--tile-size <number>', 'tile size in pixels', 256)
  .option('--resizes <number>', 'half the image this many times unless it sooner fits in one tile')
  // Supports 0.5 and 2 as the same number. 1/2 = 0.5 and 1/0.5 = 2
  .option('--resize-factor <number>', 'amount to scale down the image on each resize step', 0.5)
  .option('--dry-run', 'does all the work without writing to disk')
  .option('--no-confirm', 'remove the interactive Y/n confirmation')
  .option('--fill <R,G,B,A>', 'colour to fill the overshoot')
  .option('-v, --verbose', 'print the paths of each tile');

// Not supporting:

// - Rectangle tiles. This would mean supporting ambiguous input like '10x20'
// - Tile overlap. It's easy to support but I don't need it
// - Different origins. Right now it's _always_ top-left
// - Initial offset; i.e where to start reading
//   - This is implemented in crop() but it's ambiguous. Is there an end offset?
//   - Makes it weird to do tile count calculations
// - Initial zoom;  i.e where to resize to _before_ anything else
//   - Makes calculations really weird
//   - Just do two passes through the program or use GIMP/etc
// - Zoom steps other than 0,1,2...

program.on('--help', function () {
  console.log();
  console.log(stripIndent`
    Examples:
      $ ./dicehalf -i image.png
      $ ./dicehalf -i image.png --resizes 3 --output-spec ./out/{z}/tile-{x}-{y}.png

      Tile only
      $ ./dicehalf -i image.png --resizes 0

      Resize only
      $ ./dicehalf -i image.png --tile-size 0 --resizes 4 --output-spec ./out/size-{z}.png
  `);
  console.log();
});

// TODO: Maybe replace by `program.args.length` but it didn't work when I tried
program.on('command:*', function () {
  // If nothing was passed in
  if (process.argv.length === 2) {
    program.outputHelp();
    process.exit(0);
  }
});

program.parse(process.argv);

// By now we at least have a valid --image. Let's do the other ones
const tileSize = Number(program.tileSize);

const resizesWanted = 'resizes' in program
  ? Number(program.resizes)
  : Infinity;

if (tileSize === 0 && resizesWanted === Infinity) {
  console.error(oneLine`
    You must specify a resize limit when tile size is zero, else it'll resize
    infinitely (realistically to a 0px image)
  `);
  process.exit(1);
}

const rF = Number(program.resizeFactor);
if (rF <= 0 || rF === 1) {
  console.error('Resize factor cannot be 1, 0, or negative');
  process.exit(1);
}
const resizeFactor = rF > 1
  ? 1 / rF
  : rF;

// TODO: Path hell?
const basePath = path.resolve(__dirname, '..');
const imagePath = path.join(basePath, program.image);
const outputSpec: string = program.outputSpec;
if (program.verbose) {
  console.log(`Reading image '${imagePath}'`);
}
const originalPNG = fs.readFileSync(imagePath);
const decoded = decode(originalPNG);
const { width: originalWidth, height: originalHeight } = decoded;

// Now Typescript is happy with this as a Uint8
if (decoded.data instanceof Uint16Array) {
  throw new Error('PNG must be 8bit, was given 16');
}

const srcImage: TImage = {
  width: originalWidth,
  height: originalHeight,
  // Expensive to run `Uint8ClampedArray.from(data)`
  data: decoded.data as unknown as Uint8ClampedArray,
  channels: decoded.channels,
};

const fillColour: number[] = 'fill' in program
  ? program.fill.split(',').map(Number)
  : [0, 0, 0, 0];
while (fillColour.length < srcImage.channels) fillColour.push(0);

const zoomLevels: { resolution: string, tiles: number }[] = [];
let [w, h] = [originalWidth, originalHeight];
do {
  zoomLevels.push({
    resolution: `${w}x${h}`,
    tiles: tileSize !== 0
      ? Math.ceil(w / tileSize) * Math.ceil(h / tileSize)
      : 0,
  });
  w = Math.ceil(w * resizeFactor);
  h = Math.ceil(h * resizeFactor);
} while (zoomLevels.length <= resizesWanted && (w > tileSize || h > tileSize));

const resizesUsed = zoomLevels.length - 1;
const tiles = zoomLevels.reduce((sum, z) => sum + z.tiles, 0);

console.log(stripIndent`
  Image: ${srcImage.width}x${srcImage.height}px; ${srcImage.channels} channels
  Tile size: ${tileSize}px
  Resize factor: ${resizeFactor}%
  Resizes: ${resizesUsed} ${
  Number.isFinite(resizesWanted) && resizesWanted !== resizesUsed
    ? `(of ${resizesWanted} requested)`
    : ''}
  Zoom levels: ${zoomLevels.length}
  ${zoomLevels
    ? zoomLevels.map(z => `  - ${z.resolution}; ${z.tiles} tiles`).join('\n  ')
    : ''}
  Tiles: ${tiles}
  Overshoot fill colour: ${fillColour}
  Writing to: ${path.resolve(basePath, outputSpec)}
`);

// Store the filesize of all tiles
let size = 0;

(async () => {
  if (program.confirm) {
    const result = await promisify(read)({
      prompt: 'Is this ok? [Y/n] ',
    });
    if (result !== '' && !/y(?:es)?/i.test(result)) {
      console.log('Bye');
      return;
    }
  }
  console.log('Working...');
  for (let z = 0, resizesLeft = resizesWanted; ; z++ , resizesLeft--) {
    const { width, height } = srcImage;

    // Support resize-only with no tile generation. Templates x/y are zero
    if (!tileSize) {
      writeImageToSpec(srcImage, 0, 0, z);
    } else {
      for (let x = 0, px = 0; px < width; x++ , px += tileSize) {
        for (let y = 0, py = 0; py < height; y++ , py += tileSize) {
          const tile = crop(srcImage, px, py, tileSize, tileSize, fillColour);
          writeImageToSpec(tile, x, y, z);
        }
      }
    }
    // Support tile-only with no resizes
    if (resizesLeft === 0) break;

    const rsWidth = Math.ceil(width * resizeFactor);
    const rsHeight = Math.ceil(height * resizeFactor);
    // Is it worth it to resize?
    if (rsWidth < tileSize && rsHeight < tileSize) break;

    if (program.verbose) {
      console.log();
      console.log(`Resizing ${width}x${height} to ${rsWidth}x${rsHeight}`);
    }
    const resized = resize(srcImage, rsWidth, rsHeight);
    srcImage.data = resized.data as unknown as Uint8ClampedArray;
    srcImage.width = rsWidth;
    srcImage.height = rsHeight;
  }
  console.log();
  console.log('Generating a Leaflet app and writing to index.html');
  writeLeafletTemplate();

  console.log(
    `${tiles} tiles & ${resizesUsed} resizes is ${prettyBytes(size)}`);

  if (program.dryRun) {
    console.log();
    console.log('DRY RUN: Nothing was written to disk');
  }
})();

// TODO: PERF: Better to enqueue FS operations and perform the minimum set?
function writeImageToSpec(image: TImage, x: number, y: number, z: number) {
  const encodedPNG = encode({
    ...image,
    // Type issue
    data: image.data as unknown as Uint8Array,
  });
  size += encodedPNG.length;

  const coords = { x, y, z };
  const outpath = outputSpec.replace(/{(\w+)}/g, (str, key: string) => {
    if (key === 'x' || key === 'y' || key === 'z') {
      return String(coords[key]);
    }
    console.warn(oneLine`
      Failed to template {${key}} in '${outputSpec}'; is it not x/y/z?`);
    return str;
  });
  if (program.verbose) {
    console.log(outpath);
  }
  if (program.dryRun) {
    return;
  }
  const { dir } = path.parse(outpath);
  fs.mkdirSync(dir, { recursive: true });
  // Don't even wait synchronously. Just send it. Node's good at that
  fs.writeFile(outpath, encodedPNG, () => { });
}

function writeLeafletTemplate() {
  const scale = tileSize / originalHeight;
  console.log(`Leaflet scale is ${scale.toFixed(3)}`);
  if (program.dryRun) return;

  fs.writeFileSync('index.html', html`
    <!DOCTYPE html>
    <html>
    <head>
      <title>Dicehalf of ${imagePath}</title>
      <meta charset='utf-8' />
      <meta name='viewport' content='width=device-width, initial-scale=1.0'>
      <link
        rel='stylesheet'
        href='https://unpkg.com/leaflet@1.6.0/dist/leaflet.css'
        integrity='sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=='
        crossorigin='' />
      <script
        src='https://unpkg.com/leaflet@1.6.0/dist/leaflet.js'
        integrity='sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=='
        crossorigin=''></script>
      <style>
        .leaflet-tile {
          border: 1px dashed;
        }
        /* TODO: This doesn't work */
        .leaflet-tile:hover {
          filter: brightness(10%);
        }
      </style>
    </head>
    <body>
      <div id='map' style='height: 400px;'></div>
      <p>This is displayed using ${tiles} tiles of ${tileSize}px<sup>2</sup> each</p>
      <p>There are ${resizesUsed + 1} zoom levels</p>
      <script>
        const bounds = new L.LatLngBounds([
          [0, 0],
          [${originalHeight}, ${originalWidth}],
        ]);
        const LMap = L.map('map', {
          crs: Object.assign({}, L.CRS.Simple, {
            transformation: new L.Transformation(${scale}, 0, ${scale}, 0),
          }),
          maxBounds: bounds.pad(0.75),
        });
        L.tileLayer('${program.outputSpec}', {
          bounds,
          tileSize: ${tileSize},
          noWrap: true,
          maxZoom: ${resizesUsed},
          minZoom: 0,
          zoomReverse: true,
        }).addTo(LMap);
        L.rectangle(bounds, {
          color: 'red',
          weight: 1,
          fill: false
        }).addTo(LMap);
        LMap.fitBounds(bounds);
      </script>
    </body>
    </html>
  `);
}
