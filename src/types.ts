// Each of fast-png and resize-imagedata modules create their own Typescript
// interface for an images. They're both valid. I'll be settling on the lowest
// common denominator of them

export type TImage = {
  // This is modeled after ImageData since I want it to run in the browser
  data: Uint8ClampedArray
  width: number;
  height: number;
  // Imagedata bit depth is fixed at 8bits
  // Supports grayscale and colour so 1-4 as: G, GA, RGB, RGBA
  channels: number;
};
