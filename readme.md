# Dicehalf

Typescript application for pyramidally tiling images for use in maps like
Leaflet. https://en.wikipedia.org/wiki/Tiled_web_map.

It's repeated crop and resize. The image is diced into 256px<sup>2</sup> tiles,
then resized to half its size. Repeat this until you're happy with the minimum
zoom level.

If the image doesn't fit nicely in the tiles, it can be extended with a
background colour.

## How to

Dicehalf is a CLI script, so you'll need to run the following in either your
terminal (Linux/Mac) or Powershell on Windows.

### Binary

If you don't know anything about Node.js and don't care to, then install the
binary here: https://kudasai.xyz/res/dicehalf-x86_64 on Linux and
https://kudasai.xyz/res/dicehalf-x86_64.exe on Windows

Daily reminder to not run random binaries on your machine :^)

This one is safe but just saying.

### Source

```sh
git clone https://gitlab.com/nthm/461 dicehalf
cd dicehalf
npm install
# To run
npm run start
# With arguments
npm run start -- -i image.png ...

# To create a binary
npm run build
npx rollup out/index.js --file out/bundle.js --format cjs
npx nexe out/bundle.js --name dicehalf

./dicehalf -i image.png
```

That wasn't so bad

## Options

```
./dicehalf
Usage: dicehalf -i <image> [options]

Options:
  -V, --version             output the version number
  -i, --image <file>        image to work on (in PNG format)
  -o, --output-spec <spec>  output path stucture for tiles. Supported tokens are {x} {y} {z} (default:
                            "tiles/{z}/{y}/{x}.png")
  --tile-size <number>      tile size in pixels (default: 256)
  --resizes <number>        half the image this many times unless it sooner fits in one tile
  --resize-factor <number>  amount to scale down the image on each resize step (default: 0.5)
  --dry-run                 does all the work without writing to disk
  --no-confirm              remove the interactive Y/n confirmation
  --fill <R,G,B,A>          colour to fill the overshoot
  -v, --verbose             print the paths of each tile
  -h, --help                output usage information

Examples:
  $ ./dicehalf -i image.png
  $ ./dicehalf -i image.png --resizes 3 --output-spec ./out/{z}/tile-{x}-{y}.png

  Tile only
  $ ./dicehalf -i image.png --resizes 0

  Resize only
  $ ./dicehalf -i image.png --tile-size 0 --resizes 4 --output-spec ./out/size-{z}.png
```
